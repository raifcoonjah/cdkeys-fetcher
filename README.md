# CDKeys fetcher

### Warning
This project is **NOT** related to `cdkeys.com` or any of its subsidiaries. 

---

## Aim
CDKeys fetcher is an open source project that fetches the following information from the `cdkeys.com/clearance` page and displays them using a discord bot/Webhook. This project was made to be selfhosted, **no demo version will ever be available**. 
